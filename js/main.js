/**
 * Created by ezgoing on 14/9/2014.
 */
'use strict';

$(function(){
	$('#enviaLogin').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		$.post('dologin.php', $('#formLogin').serialize(), function(data){
			
			if(data == "sucesso"){
				window.location.href = 'dolist.php';
			}else{
				alert(data);
			}
		}).error(function(data) { console.log(data.responseText); alert("Ocorreu um erro inesperado no sistema. Por favor contato o desenvolvedor."); });
	});



	
	document.querySelector('#file').addEventListener('change', function(){
        
		var file    = document.querySelector('#file').files[0];
        var reader = new FileReader();
        
        reader.onload = function(e) {
            var imgSrc = e.target.result;
            var img = '<img src="'+imgSrc+'">';
            $('.imageBox').append(img);
        }

        if (file) {
           reader.readAsDataURL(file); //reads the data as a URL
       	} else {
           preview.src = "";
       	}
        
    });


	$("#gerarImagem").click(function() { 
        html2canvas($("#ct-imagem"), {
            onrendered: function(canvas) {
                var theCanvas = canvas;
                //document.body.appendChild(canvas);

                canvas.toBlob(function(blob) {
					saveAs(blob, "Dashboard.png"); 
				});
            }
        });
    });



});
