-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.14 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para imagem_pv
CREATE DATABASE IF NOT EXISTS `imagem_pv` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `imagem_pv`;

-- Copiando estrutura para tabela imagem_pv.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  `senha` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela imagem_pv.usuarios: 7 rows
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `nome`, `login`, `senha`) VALUES
	(1, 'SEB COC Ribeirão', 'sebribeirao', 'e1c09f3e336b374c5c91d37a97e32ad0'),
	(2, 'SEB COC Rio Preto', 'sebriopreto', 'e1c09f3e336b374c5c91d37a97e32ad0'),
	(3, 'SEB THATHI COC', 'sebthathi', 'e1c09f3e336b374c5c91d37a97e32ad0'),
	(4, 'SEB Dinatos COC', 'sebdinatos', 'e1c09f3e336b374c5c91d37a97e32ad0'),
	(5, 'SEB COC Vitoria', 'sebvitoria', 'e1c09f3e336b374c5c91d37a97e32ad0'),
	(6, 'Dom Bosco', 'dombosco', 'e1c09f3e336b374c5c91d37a97e32ad0'),
	(7, 'GEO', 'geo', 'e1c09f3e336b374c5c91d37a97e32ad0');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
