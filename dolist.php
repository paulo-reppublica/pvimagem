<?php

session_start();


if( isset( $_SESSION["login"] )){
    if( $_SESSION["login"] == '' ){
        header('Location: index.php');
    }
}else{
    header('Location: index.php');
}

$usuario = $_SESSION["login"];
$imagem = 'img/frames/post-verde.png';
$logo = 'img/logos/logo-ribeirao.png';

switch($usuario){
    case '"sebribeirao"': $imagem = 'img/frames/post-2017.png'; $logo = 'img/logos/logo-ribeirao.png'; break;
    case '"sebriopreto"': $imagem = 'img/frames/post-2017.png'; $logo = 'img/logos/logo-riopreto.png'; break;
    case '"sebthathi"': $imagem = 'img/frames/post-thathi-2017.png'; $logo = 'img/logos/logo-thathi.png'; break;
    case '"sebdinatos"': $imagem = 'img/frames/post-2017.png'; $logo = 'img/logos/logo-dinatos.png'; break;
    case '"sebvitoria"': $imagem = 'img/frames/post-2017.png'; $logo = 'img/logos/logo-vitoria.png'; break;
    case '"sebmaceio"': $imagem = 'img/frames/post-2017.png'; $logo = 'img/logos/logo-maceio.png'; break;
    case '"sebvilavelha"': $imagem = 'img/frames/post-2017.png'; $logo = 'img/logos/logo-vilavelha.png'; break;
    case '"dombosco"': $imagem = 'img/frames/post-2017.png'; $logo = 'img/logos/logo-dombosco.png'; break;
    case '"geo"': $imagem = 'img/frames/post-2017.pngg'; $logo = 'img/logos/logo-geo.png'; break;
    case '"sartre"': $imagem = 'img/frames/post-2017.png'; $logo = 'img/logos/logo-sartre.png'; break;
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Gerador de Imagem Facebook</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700|Pacifico|Covered+By+Your+Grace" rel="stylesheet"> 

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


  </head>
  <body>
    
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <a class="navbar-brand text-uppercase" href="#">Gerador de Imagem Facebook</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
        
        
          </ul>
          
          <ul class="nav navbar-nav navbar-right">
            <li><a href="dologout.php"><span class="glyphicon glyphicon-log-out"></span> Sair</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <div id="ct-imagem" class="ct-imagem" style="background: url('<?php echo $imagem; ?>') no-repeat center;">
    
    <?php if ($usuario=='"sebthathi"') {?>
      
      <div class="imageBox Thathi">
        
        <input type="file" id="file" class="file">
      </div>
      <input type="text" id="nome" class="nomeThathi" value="João Carlos Albuquerque" >    

      <input type="text" id="curso" class="cursoThathi" value="Medicina - USP São Carlos">

      <div class="ct-logo">
        <img src="<?php echo $logo; ?>" class="logo-escolaThathi">
      </div>

    <?php }else{?>
      <div class="imageBox">
          
          <input type="file" id="file" class="file">
      </div>

      <input type="text" id="nome" class="nome" value="João Carlos Albuquerque" >    

      <input type="text" id="curso" class="curso" value="Medicina - USP São Carlos">

      <div class="ct-logo">
        <img src="<?php echo $logo; ?>" class="logo-escola">
      </div>

    <?php }?>
    

    </div>    

    <button type="button" id="gerarImagem" class="btn btn-primary btn-lg">Gerar Imagem</button>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/html2canvas.js"></script>
    <script src="js/filesaver.js"></script>
    <script src="js/main.js"></script>
    

   

  </body>
</html>